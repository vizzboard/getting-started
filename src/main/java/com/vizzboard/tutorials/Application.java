package com.vizzboard.tutorials;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private VizzboardService vizzboardService;

    @Override
    public void run(String... args) {
        List<Dashboard> dashboards = this.vizzboardService.getDashboards();

        dashboards.forEach(d -> System.out.println(d.getTitle()));
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
