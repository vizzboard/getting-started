package com.vizzboard.tutorials;

import com.auth0.jwt.JWTSigner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class VizzboardService {

    @Value("${apiKey}")
    private String apiKey;

    @Value("${apiSecret}")
    private String apiSecret;

    @Value("${userName}")
    private String userName;

    @Value("${baseUrl}")
    private String baseUrl;

    private RestTemplate restTemplate = new RestTemplate();

    public List<Dashboard> getDashboards() {
        String token = this.getToken();
        String url = baseUrl + "/content";

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authentication", "Bearer " + token);

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<List<Dashboard>> rateResponse = restTemplate.exchange(url,
                HttpMethod.GET, entity, new ParameterizedTypeReference<List<Dashboard>>() {
                });

        return rateResponse.getBody();
    }

    private String getToken() {
        Map<String, Object> body = new HashMap<>();

        body.put("apiKey", apiKey);
        body.put("userName", userName);

        return new JWTSigner(apiSecret).sign(body);
    }
}
